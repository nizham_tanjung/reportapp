import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Area } from './area';

let httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + localStorage.getItem('apptoken'),
  })
};

@Injectable({
  providedIn: 'root'
})
export class AreaService {

  protected url: string = "http://202.157.186.30:8000/area";

  constructor(private http: HttpClient) {
  }

  getAreas() {
    return this.http.get<Area[]>(this.url, httpOptions);
  }

  getArea(areaid: number) {
    let customUrl = this.url + "/" + areaid;
    return this.http.get<Area>(customUrl, httpOptions);
  }

  saveArea(area: Area) {
    area.default_area = false;
    this.http.post<Area>(this.url, area, httpOptions).subscribe({
      next: data => area.id = data.id,
      error: error => console.error('There was an error when saving Area!', error)
    });
  }

  updateArea(area: Area) {
    area.default_area = false;
    this.http.put<Area>(this.url, area, httpOptions).subscribe({
      next: data => area.id = data.id,
      error: error => console.error('There was an error when updating area!', error)
    });
  }

  deleteArea(areaid: number) {
    this.http.delete<Area>(this.url + "/del/" + areaid, httpOptions).subscribe({
      error: error => console.log('There was an error when deleting area!', error)
    });
  }

}
