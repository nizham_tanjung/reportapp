import { SafeHtml } from '@angular/platform-browser';

export interface Area {
  id: number;
  code: SafeHtml;
  name: SafeHtml;
  parent: SafeHtml;
  default_area: boolean;
}
