import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Area } from './area';
import { AreaService } from './area.service';

@Component({
  selector: 'app-areas',
  templateUrl: './areas.component.html',
  styleUrls: ['./areas.component.scss']
})
export class AreasComponent implements OnInit {
  area: Area;
  restArea: Area[] = [];
  title = 'Area - Depok';
  formTitle = 'Add Area';

  displayBasic = false;
  confirmDel = "";
  isSaveVisible = "";
  isUpdateVisible = "";
  isDelVisible = "";

  constructor(private areaService: AreaService, private router: Router) {
    this.area = {
      id: 0,
      code: '',
      name: '',
      parent: '',
      default_area: false
    }
  }

  getRestArea(areaid: number) {
    this.areaService.getArea(areaid).subscribe((area) => {
      this.area.id = area.id;
      this.area.code = area.code;
      this.area.name = area.name;
      this.area.parent = area.parent;
      this.area.default_area = false;
    });
  }

  getRestAreas() {
    this.areaService.getAreas().subscribe((areas) => {
      this.restArea = areas;
    });
  }

  saveRestArea() {
    this.areaService.saveArea(this.area);
    this.clearDialog();
    this.getRestAreas();
  }

  updateRestArea() {
    this.areaService.updateArea(this.area);
    this.clearDialog();
    this.displayBasic = false;
    this.getRestAreas();
  }

  deleteRestArea() {
    this.areaService.deleteArea(this.area.id);
    this.clearDialog();
    this.displayBasic = false;
    this.getRestAreas();
  }

  showDialog() {
    this.clearDialog();
    this.isSaveVisible = "";
    this.isUpdateVisible = "hidden";
    this.isDelVisible = "hidden";
    this.formTitle = 'Add Area';
    this.confirmDel = "";
    return this.displayBasic = true;
  }

  showDialogUpdate(areaid: number, btn: string) {
    this.getRestArea(areaid);
    if (btn === "btnEdit") {
      this.formTitle = 'Edit Area';
      this.isSaveVisible = "hidden"
      this.isUpdateVisible = "";
      this.isDelVisible = "hidden";
      this.confirmDel = "";
    } else if (btn === "btnDelete") {
      this.formTitle = 'Delete Area';
      this.isSaveVisible = "hidden"
      this.isUpdateVisible = "hidden";
      this.isDelVisible = "";
      this.confirmDel = " - Are you sure to delete this area?";
    }
    return this.displayBasic = true;
  }

  hideDialog() {
    return this.displayBasic = false;
  }

  clearDialog() {
    this.area = {
      id: 0,
      code: "",
      name: "",
      parent: "",
      default_area: false
    }
  }

  ngOnInit(): void {

  }

}
