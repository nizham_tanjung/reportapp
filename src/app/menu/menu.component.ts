import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { Account } from '../accounts/account';
import { AccountsService } from './../accounts/accounts.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, OnDestroy {

  title = 'ReportApp';
  items: MenuItem[] = [];
  appuser: String = "";
  approle: String = "";
  apptoken: String = "";
  account: Account;

  constructor(private router: Router, private http: HttpClient, private accService: AccountsService) {
    this.account = {
      id: 0,
      email: "",
      username: "",
      password: "",
      role: "",
      area: ""
    }
  }

  @HostListener("unloaded")
  ngOnDestroy(): void {
    this.appuser = "";
    this.approle = "";
    console.log("menu out");
  }

  manageMenu() {
    if (this.approle === "admin") {
      this.items = [
        {
          label: 'Accounts',
          icon: 'pi pi-fw pi-id-card',
          routerLink: ['/accounts']
        },
        {
          label: 'Area',
          icon: 'pi pi-fw pi-map-marker',
          routerLink: ['/areas']
        },
        {
          label: 'Create Report',
          icon: 'pi pi-fw pi-file',
          routerLink: ['/create_report']
        },
        {
          label: 'View Report',
          icon: 'pi pi-fw pi-bars',
          routerLink: ['/reports']
        }
      ];
    } else if (this.approle === "reporter") {
      this.items = [
        {
          label: 'Create Report',
          icon: 'pi pi-fw pi-file',
          routerLink: ['/create_report']
        },
        {
          label: 'View Report',
          icon: 'pi pi-fw pi-bars',
          routerLink: ['/reports']
        }
      ];
    } else if (this.approle === "viewer") {
      this.items = [
        {
          label: 'View Report',
          icon: 'pi pi-fw pi-bars',
          routerLink: ['/reports']
        }
      ];
    }
  }

  ngOnInit(): void {
    this.appuser = String(localStorage.getItem("appusername"));
    this.apptoken = String(localStorage.getItem("apptoken"));
    this.approle = String(localStorage.getItem("approle"));

    console.log("From init: user - " + this.appuser + " | role - " + this.approle);
    this.manageMenu();
  }

  profile(): void {
    this.router.navigate(['/profiles']);
  }

  logout(): void {
    localStorage.clear();
    this.router.navigate(['/']);
  }
}
