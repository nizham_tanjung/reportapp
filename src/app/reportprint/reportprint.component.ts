import { ReportFile } from './../reports/reportfile';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Report } from '../reports/report';
import { ReportService } from '../reports/report.service';

@Component({
  selector: 'app-reportprint',
  templateUrl: './reportprint.component.html',
  styleUrls: ['./reportprint.component.scss']
})
export class ReportprintComponent implements OnInit, OnDestroy {

  id: string = "0";
  private sub: any;
  report: Report;
  reportFiles: ReportFile[] = []
  reporter: string = "";
  fileurls: string[] = [];

  constructor(private activeRoute: ActivatedRoute, private reportServ: ReportService) {
    this.report = {
      id: 0,
      category: "",
      what: "",
      when: "",
      why: "",
      who: "",
      where: "",
      how: "",
      area: "",
      classified: false,
      report_by: ""
    }

    this.sub = this.activeRoute.params.subscribe(param => {
      this.id = param['id'];
    });
  }

  getReport() {
    this.reportServ.getReportById(this.id).subscribe(rep => {
      this.report = rep;
      console.log(this.report);
    });
    this.getReportImages();
  }

  getReportImages() {
    this.reportServ.getReportFiles(this.id).subscribe(images => {
      for (let i = 0; i < images.length; i++) {
        let filename = images[i].file_name;
        let path = images[i].file_path;
        this.fileurls.push('http://localhost:8000/' + path + filename);
      }
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  ngOnInit(): void {
    this.sub = this.activeRoute.params.subscribe(param => {
      this.id = param['id'];
      this.getReport();
    });
    if (localStorage.getItem("appusername") !== null) {
      this.reporter = String(localStorage.getItem("appusername"));
    }
  }

  goBack(): void {
    window.history.back();
  }
}
