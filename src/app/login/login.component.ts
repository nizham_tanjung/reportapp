import { LocationStrategy } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Account } from '../accounts/account';
import { AccountsService } from './../accounts/accounts.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  message: string = '';
  form: FormGroup;

  constructor(private formBuilder: FormBuilder, private http: HttpClient,
    private router: Router, private locationStrategy: LocationStrategy, private accService: AccountsService) {
    this.form = formBuilder.group({
      username: '',
      password: ''
    });
  }

  preventBackButton() {
    history.pushState(null, "", location.href);
    this.locationStrategy.onPopState(() => {
      history.pushState(null, "", location.href);
    })
  }

  ngOnInit(): void {
    this.preventBackButton();
  }

  submit(): void {
    this.http.post<any>("http://202.157.186.30:8000/login", this.form.getRawValue())
      .subscribe((result: any) => {
        this.message = `You're logged in`;
        localStorage.setItem("isAuthenticated", "ya");
        localStorage.setItem("apptoken", result.access_token);

        let appuser: string = this.form.controls['username'].value;
        localStorage.setItem("appusername", appuser);

        let url: string = "http://202.157.186.30:8000/account" + "/name/" + appuser;
        let httpOptions = {
          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + result.access_token,
          })
        };
        this.http.get<Account>(url, httpOptions).subscribe((data) => {
          localStorage.setItem('approle', String(data.role));
          localStorage.setItem('apparea', String(data.area));
        });

        this.router.navigate(['/reports']);
      }, err => {
        this.message = "You are not logged in";
      });
  }

}
