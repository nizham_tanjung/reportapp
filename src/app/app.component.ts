import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ReportApp';
  visible = false;

  constructor(private router: Router) {
    router.events.subscribe((val) => {
      if (localStorage.getItem("isAuthenticated")) {
        this.visible = true;
      } else {
        this.visible = false;
      }
    });
  }

  ngOnInit(): void {
    // Emitters.authEmitter.subscribe((auth: boolean) => { this.authenticated = auth })
    if (localStorage.getItem("isAuthenticated")) {
      this.visible = true;
    } else {
      this.visible = false;
    }
  }
}
