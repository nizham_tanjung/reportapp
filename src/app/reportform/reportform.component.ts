import { ReportFile } from './../reports/reportfile';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Area } from './../areas/area';
import { AreaService } from './../areas/area.service';
import { ReportService } from './../reports/report.service';
import { ReportForm } from './reportform';

@Component({
  selector: 'app-reportform',
  templateUrl: './reportform.component.html',
  styleUrls: ['./reportform.component.scss'],
})
export class ReportformComponent implements OnInit {

  appuser = localStorage.getItem("appusername");
  apparea = localStorage.getItem("apparea");
  text = "";
  selDate = new Date();
  categories: any[] = [
    { name: '--Choose Category--', key: '' },
    { name: 'Ideologi', key: 'Ideologi' },
    { name: 'Politik', key: 'Politik' },
    { name: 'Ekonomi', key: 'Ekonomi' },
    { name: 'Sosial', key: 'Sosial' },
    { name: 'Budaya', key: 'Budaya' },
    { name: 'Hankam', key: 'Hankam' },
  ];
  uploadedFiles: String[] = [];
  restArea: Area[] = [];
  restReport: ReportForm[] = [];
  authenticated = false;
  report: ReportForm;
  reportfile: ReportFile;
  area: Area;
  confidential: boolean = false;
  urls: any[] = [];
  msg = "";

  constructor(private router: Router, private areaSrv: AreaService,
    private reportSrv: ReportService, private messageService: MessageService) {
    this.report = {
      id: 0,
      category: "",
      what: "",
      when: "",
      why: "",
      who: "",
      where: "",
      how: "",
      area: "",
      classified: false,
      report_by: ""
    }
    this.area = {
      id: 0,
      code: "",
      name: "",
      parent: "",
      default_area: false
    }
    this.reportfile = {
      id: 0,
      report_id: "",
      file_name: "",
      file_path: ""
    }
  }

  ngOnInit(): void {
    this.grabAreas();
  }

  grabAreas() {
    this.areaSrv.getAreas().subscribe((areas) => {
      this.restArea = areas;
    });
  }

  // allReport(area: string) {
  //   this.reportSrv.getReportByArea(area)
  // }

  clearReport() {
    this.report = {
      id: 0,
      category: "",
      what: "",
      when: "",
      why: "",
      who: "",
      where: "",
      how: "",
      area: "",
      classified: false,
      report_by: ""
    }
    this.confidential = false;
    this.uploadedFiles = [];
    this.urls = [];
    this.msg = "";
  }

  clearArea() {
    this.area = {
      id: 0,
      code: "",
      name: "",
      parent: "",
      default_area: false
    }
  }

  notifySaving() {
    this.messageService.add({ severity: 'success', summary: 'Successfully Save Report', detail: '' });
  }

  saveReport() {
    let selMonth = this.selDate.getMonth() + 1;
    let convMon = '';
    if (selMonth < 10) {
      convMon = '0' + selMonth
    } else {
      convMon = selMonth.toString()
    }
    this.report.when = this.selDate.getFullYear() + '-' + convMon + '-' + this.selDate.getDate();
    if (this.appuser != null) {
      this.report.report_by = this.appuser
    } else {
      this.report.report_by = ""
    }
    this.reportSrv.saveReport(this.report).subscribe(data => {
      let reportid = Number(data);
      let filelen = localStorage.getItem('tempfilelen');
      if (filelen != undefined || filelen != null) {
        for (let i = 0; i < Number(filelen); i++) {
          let filename = localStorage.getItem('tempfile' + i);
          this.reportfile = {
            id: 0,
            report_id: reportid,
            file_name: String(filename),
            file_path: 'dataimg/'
          }
          this.reportSrv.saveReportFile(this.reportfile);
          localStorage.removeItem('tempfile' + i);
        }
        localStorage.removeItem('tempfilelen');
      }
    });
    this.clearReport();
    this.clearArea();
    this.notifySaving();
  }

  myUploader(event: any) {
    localStorage.setItem('tempfilelen', event.files.length)
    for (let i = 0; i < event.files.length; i++) {
      this.reportSrv.upload(event.files[i]);
      localStorage.setItem("tempfile" + i, event.files[i].name);
    }
    this.messageService.add({ severity: 'info', summary: 'Files accepted', detail: '' });
  }
  // -- end of image processing --
}
