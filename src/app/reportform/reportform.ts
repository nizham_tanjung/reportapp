import { SafeHtml } from '@angular/platform-browser';

export interface ReportForm {
  id: number;
  category: SafeHtml;
  what: SafeHtml;
  when: SafeHtml;
  why: SafeHtml;
  who: SafeHtml;
  where: SafeHtml;
  how: SafeHtml;
  area: SafeHtml;
  classified: boolean;
  report_by: string;
}
