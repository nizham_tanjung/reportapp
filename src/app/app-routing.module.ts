import { ReportprintComponent } from './reportprint/reportprint.component';
import { ProfilesComponent } from './profiles/profiles.component';
import { ReportformComponent } from './reportform/reportform.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountsComponent } from './accounts/accounts.component';
import { AreasComponent } from './areas/areas.component';
import { LoginComponent } from './login/login.component';
import { ReportsComponent } from './reports/reports.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'logout', component: LoginComponent },
  { path: 'reports', component: ReportsComponent },
  { path: 'create_report', component: ReportformComponent },
  { path: 'areas', component: AreasComponent },
  { path: 'accounts', component: AccountsComponent },
  { path: 'printable/:id', component: ReportprintComponent },
  { path: 'profiles', component: ProfilesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
