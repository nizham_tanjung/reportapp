import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Report } from './report';
import { ReportFile } from './reportfile';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + localStorage.getItem('apptoken')
  })
};

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  protected url: string = "http://202.157.186.30:8000/report";

  constructor(private http: HttpClient) { }

  getReports() {
    return this.http.get<Report[]>(this.url, httpOptions).pipe(map(data => data));
  }

  getReportAreaNoDate(area_code: string) {
    let customUrl = this.url + "/by_area/" + area_code;
    return this.http.get<Report[]>(customUrl, httpOptions).pipe(map(data => data));
  }

  getReportByArea(area_code: string, chosen_date: string) {
    let customUrl = this.url + "/area/" + area_code + "/date/" + chosen_date;
    return this.http.get<Report[]>(customUrl, httpOptions).pipe(map(data => data));
  }

  getSecretReport(area_code: string, chosen_date: string) {
    let customUrl = this.url + "/sec_area/" + area_code + "/sec_date/" + chosen_date;
    return this.http.get<Report[]>(customUrl, httpOptions).pipe(map(data => data));
  }

  getReportById(reportid: string) {
    let customUrl = this.url + "/" + reportid;
    return this.http.get<Report>(customUrl, httpOptions).pipe(map(data => data));
  }

  saveReport(report: Report) {
    return this.http.post<Report>(this.url, report, httpOptions);
  }

  upload(file: File) {
    let privateUrl = 'http://202.157.186.30:8000/report_files/uploadfile/';
    let formData: FormData = new FormData();
    formData.append("file", file);
    this.http.post<File>(privateUrl, formData).subscribe({
      next: data => console.log(data),
      error: error => console.error(error)
    });
  };

  updateReport(report: Report) {
    return this.http.put<Report>(this.url, report, httpOptions);
  }

  deleteReport(reportid: number) {
    return this.http.delete<Report>(this.url + "/del/" + reportid, httpOptions);
  }

  saveReportFile(reportfile: ReportFile) {
    let privateUrl = 'http://202.157.186.30:8000/report_files/register/';
    this.http.post<ReportFile>(privateUrl, reportfile, httpOptions).subscribe({
      next: data => reportfile.id = data.id,
      error: error => console.error('There was an error when saving Report!', error)
    });
  }

  getReportFiles(reportId: string) {
    let privateUrl = 'http://202.157.186.30:8000/report_files/report/' + reportId;
    return this.http.get<ReportFile[]>(privateUrl, httpOptions);
  }
}
