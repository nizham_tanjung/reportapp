import { SafeHtml } from '@angular/platform-browser';

export interface ReportFile {
  id: number;
  report_id: SafeHtml;
  file_name: SafeHtml;
  file_path: SafeHtml;
}
