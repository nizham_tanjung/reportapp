import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Area } from './../areas/area';
import { AreaService } from './../areas/area.service';
import { Report } from './report';
import { ReportService } from './report.service';

export interface Months {
  monthName: string;
  monthNum: string;
}

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})

export class ReportsComponent implements OnInit, OnDestroy {
  title = "Events & Happenings";
  report: Report;
  restReport: Report[] = [];
  restArea: Area[] = [];
  area: Area;
  monthOptions: Months[] = [];
  selectedMonth = "";
  classified = false;
  app_role = "";
  app_area = "";
  app_user = "";

  formTitle = "";
  displayBasic = false;
  isDelVisible = "";
  isUpdateVisible = "";

  categories: any[] = [
    { name: '--Choose Category--', key: '' },
    { name: 'Ideologi', key: 'Ideologi' },
    { name: 'Politik', key: 'Politik' },
    { name: 'Ekonomi', key: 'Ekonomi' },
    { name: 'Sosial', key: 'Sosial' },
    { name: 'Budaya', key: 'Budaya' },
    { name: 'Hankam', key: 'Hankam' },
  ];

  constructor(
    private router: Router, private messageService: MessageService,
    private rptService: ReportService, private areaSrv: AreaService) {

    this.report = {
      id: 0,
      category: "",
      what: "",
      when: "",
      why: "",
      who: "",
      where: "",
      how: "",
      area: "",
      classified: false,
      report_by: ""
    }

    this.area = {
      id: 0,
      code: "",
      name: "",
      parent: "",
      default_area: false
    }

    this.monthOptions = [
      { monthName: 'January', monthNum: '01' },
      { monthName: 'February', monthNum: '02' },
      { monthName: 'March', monthNum: '03' },
      { monthName: 'April', monthNum: '04' },
      { monthName: 'May', monthNum: '05' },
      { monthName: 'June', monthNum: '06' },
      { monthName: 'July', monthNum: '07' },
      { monthName: 'August', monthNum: '08' },
      { monthName: 'September', monthNum: '09' },
      { monthName: 'October', monthNum: '10' },
      { monthName: 'November', monthNum: '11' },
      { monthName: 'December', monthNum: '12' }
    ];
  }

  @HostListener("unloaded")
  ngOnDestroy(): void {
    console.log("report out");
  }

  getTheReports(area_code: string, chosen_date: string) {
    if (this.classified === false) {
      this.rptService.getReportByArea(area_code, chosen_date).subscribe((reports) => {
        this.restReport = reports;
      });
    } else {
      this.rptService.getSecretReport(area_code, chosen_date).subscribe((reports) => {
        this.restReport = reports;
      });
    }
  }

  getTheAreas() {
    this.areaSrv.getAreas().subscribe(data => {
      this.restArea = data;
    });
  }

  initData(): void {
    if (this.app_role) {
      if (this.app_role === 'admin') {
        this.rptService.getReports().subscribe(reports => {
          this.restReport = reports;
        });
      } else {
        this.rptService.getReportAreaNoDate(this.app_area).subscribe(reports => {
          this.restReport = reports;
        });
      }
    }
  }

  ngOnInit(): void {
    this.app_role = String(localStorage.getItem("approle"));
    this.app_area = String(localStorage.getItem("apparea"));
    this.app_user = String(localStorage.getItem("appusername"));
    this.getTheAreas();
    this.initData();
  }

  showDetail(reportid: string): void {
    this.router.navigate(['/printable', reportid]);
  }

  showData(): void {
    this.restReport = [];
    let stored_area = this.app_area !== null ? this.app_area.toString() : "";
    let stored_role = this.app_role;
    let chosen_area = this.area !== null ? this.area.toString() : "";
    let chosen_month = this.selectedMonth != null ? this.selectedMonth : "";

    if (stored_role === 'admin') {
      if (chosen_area === '32.76') {
        this.rptService.getReports().subscribe(reports => {
          this.restReport = reports;
        });
      } else {
        this.getTheReports(chosen_area, chosen_month);
      }
    } else {
      this.getTheReports(stored_area, chosen_month);
    }
  }

  // Action Dialog
  hideDialog() {
    this.displayBasic = false;
  }

  deleteReport() {
    this.rptService.deleteReport(this.report.id).subscribe(data => {
      this.messageService.add({ severity: 'danger', summary: 'Report Deleted', detail: '' })
      console.log(data);
      this.hideDialog();
      this.initData();
    });
  }

  updateReport() {
    this.report.report_by = this.app_user;
    this.rptService.updateReport(this.report).subscribe(data => {
      this.messageService.add({ severity: 'warning', summary: 'Report Updated', detail: '' })
      console.log(data);
      this.hideDialog();
      this.initData();
    });
  }

  showDialogUpdate(id: string, btn: string) {
    this.rptService.getReportById(id).subscribe(data => this.report = data);
    switch (btn) {
      case 'btnEdit':
        this.isDelVisible = 'hidden';
        this.isUpdateVisible = '';
        this.formTitle = 'Edit Report';
        break;
      case 'btnDelete':
        this.isDelVisible = '';
        this.isUpdateVisible = 'hidden';
        this.formTitle = 'Are you sure to delete this report ?';
        break;
      default:
        this.isDelVisible = 'hidden';
        this.isUpdateVisible = 'hidden';
        this.formTitle = '';
        break;
    }
    this.displayBasic = true;
  }
}
