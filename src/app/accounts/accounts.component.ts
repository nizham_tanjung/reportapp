import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Area } from '../areas/area';
import { AreaService } from './../areas/area.service';
import { Account } from './account';
import { AccountsService } from './accounts.service';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {

  account: Account;
  restAccount: Account[] = [];
  title = 'Accounts';
  formTitle = 'Add Account';

  displayBasic = false;
  confirmDel = "";
  isSaveVisible = "";
  isUpdateVisible = "";
  isDelVisible = "";

  roles: any[] = [
    { name: 'Administrator', key: 'admin' },
    { name: 'Reporter', key: 'reporter' },
    { name: 'Viewer', key: 'viewer' },
    { name: 'Secret Viewer', key: 'secret' }
  ];

  restArea: Area[] = []

  constructor(private router: Router, private accService: AccountsService, private areaSrv: AreaService) {
    this.account = {
      id: 0,
      email: "",
      username: "",
      password: "",
      role: "",
      area: ""
    }
    this.grabAreas();
  }

  getRestAccount(accid: string) {
    this.accService.getAccountById(accid).subscribe((account) => {
      this.account.id = account.id;
      this.account.email = account.email;
      this.account.username = account.username;
      this.account.password = account.password;
      this.account.role = account.role;
      this.account.area = account.area;
    });
  }

  getRestAccounts() {
    this.accService.getAccounts().subscribe((result) => {
      this.restAccount = result;
    });
  }

  saveRestAccount() {
    this.accService.saveAccount(this.account);
    this.clearDialog();
    this.getRestAccounts();
  }

  updateRestAccount() {
    this.accService.updateAccount(this.account);
    this.clearDialog();
    this.displayBasic = false;
    this.getRestAccounts();
  }

  deleteRestAccount() {
    this.accService.deleteAccount(this.account.id);
    this.clearDialog();
    this.displayBasic = false;
    this.getRestAccounts();
  }

  showDialog() {
    this.clearDialog();
    this.isSaveVisible = "";
    this.isUpdateVisible = "hidden";
    this.isDelVisible = "hidden";
    this.formTitle = 'Add Account';
    this.confirmDel = "";
    return this.displayBasic = true;
  }

  showDialogUpdate(accountid: number, btn: string) {
    this.getRestAccount(String(accountid));
    if (btn === "btnEdit") {
      this.formTitle = 'Edit Account';
      this.isSaveVisible = "hidden"
      this.isUpdateVisible = "";
      this.isDelVisible = "hidden";
      this.confirmDel = "";
    } else if (btn === "btnDelete") {
      this.formTitle = 'Delete Account';
      this.isSaveVisible = "hidden"
      this.isUpdateVisible = "hidden";
      this.isDelVisible = "";
      this.confirmDel = " - Are you sure to delete this account?";
    }
    return this.displayBasic = true;
  }

  hideDialog() {
    return this.displayBasic = false;
  }

  clearDialog() {
    this.account = {
      id: 0,
      email: "",
      username: "",
      password: "",
      role: "",
      area: ""
    }
  }

  grabAreas() {
    this.areaSrv.getAreas().subscribe((areas) => {
      this.restArea = areas;
    });
  }

  ngOnInit(): void {

  }

}
