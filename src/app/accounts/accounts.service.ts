import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Account } from './account';


let httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + localStorage.getItem('apptoken'),
  })
};

@Injectable({
  providedIn: 'root'
})
export class AccountsService {

  protected url: string = "http://202.157.186.30:8000/account";

  constructor(private http: HttpClient) { }

  getAccounts() {
    return this.http.get<Account[]>(this.url, httpOptions);
  }

  getAccountById(accountid: string) {
    let customUrl = this.url + "/" + accountid;
    return this.http.get<Account>(customUrl, httpOptions); //.pipe( map(data => data)
  }

  getAccountByName(accountname: string) {
    let customUrl = this.url + "/name/" + accountname;
    return this.http.get<Account>(customUrl, httpOptions); //.pipe( map(data => data)
  }

  saveAccount(account: Account) {
    this.http.post<Account>(this.url, account, httpOptions).subscribe({
      next: data => account.id = data.id,
      error: error => console.error('There was an error when saving Account!', error)
    });
  }

  updateAccount(account: Account) {
    this.http.put<Account>(this.url, account, httpOptions).subscribe({
      next: data => account.id = data.id,
      error: error => console.error('There was an error when saving Account!', error)
    });
  }

  deleteAccount(accountid: number) {
    let delUrl = this.url + "/del/" + accountid;
    console.log(delUrl);
    this.http.delete<any>(delUrl, httpOptions).subscribe({
      next: data => console.log(data),
      error: error => console.error('Error deleting account ', error)
    });
  }

}
