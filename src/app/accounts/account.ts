import { SafeHtml } from '@angular/platform-browser';

export interface Account {
  id: number;
  email: SafeHtml;
  username: SafeHtml;
  password: SafeHtml;
  role: SafeHtml;
  area: SafeHtml;
}
